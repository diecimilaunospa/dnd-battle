# Dungeons and Dragons Battle

This is a learning javascript project that use the dnd5 REST API.

## Features

-   User login with class selection and weapon selection based on class.
-   Persistent user data in local storage
-   Combat system with random mobs.
-   Leveling system (bonus)

### User login and class selection

Se e' un nuovo utente viene mostrata la ui di scelta del personaggio

1. Vengono mostrate tutte le razze e classi disponibili. L'utente puo scegliere una razza e la rispettiva classe.
2. In base alla classe scelta vengono mostrati gli equipaggiamenti iniziali disponibili.
3. L'utente inserisce il proprio username, viene salvato tutto nel local storage.

Se non e' un nuovo utente viene inviato dirrettamente alla pagina principale con le sue statistiche.

## Usefull links

-   DND5 Rest API https://www.dnd5eapi.co/
-   TailwindCSS https://tailwindcss.com/docs/installation
-   Localstorage https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
