# HTTP

## Http url schema

URL -> https://domain_name.com/endpoint?queryparams

Example:

https://www.youtube.com/watch?v=twxYAP5n-JI

-   Protocollo -> https
-   Dominio -> youtube.com
-   Sottodominio -> www
-   endpoint -> watch
-   queryparams
    -   chiave: v
    -   valore: twxYAP5n-JI

https://www.dnd5eapi.co/api/classes/paladin

-   Protocollo -> https
-   Dominio -> dnd5eapi.co
-   endpoint -> /api/classes/paladin

https://www.dnd5eapi.co/api/classes?type=paladin

-   Protocollo -> https
-   Dominio -> dnd5eapi.co
-   endpoint -> /api/classes
-   queryparms:
    chiave: type
    valore: paladin

## Verbs

-   GET (Prende informazioni non modifica o aggiunge dati)
-   POST (Creare informazioni)
-   PATCH (Altera informazioni in modo parziale, non richiede tutti i parametri)
-   PUT (Modifica informazioni, richiede tutti i parametri)
-   DELETE
