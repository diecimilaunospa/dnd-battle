/**
 *  class names: PascalCase
 *  methods: camelCase
 *  file names: kebak-case.js
 */

import { User } from "./classes.js";

const loginBox = document.getElementById("login-box");

const userNameText = document.getElementById("username-input");
const loginErrorText = document.getElementById("login-error-text");
const loginButton = document.getElementById("login-btn");
const createNewCharacterBtn = document.getElementById("create-character-btn");

const characterBox = document.getElementById("characters-box");

const chooseRaceBox = document.getElementById("choose-race-box");

const chooseClassBox = document.getElementById("choose-class-box");

const chooseEquipmentBox = document.getElementById("choose-equipment-box");

loginButton.addEventListener("click", checkExistingUser);
createNewCharacterBtn.addEventListener("click", createNewUser);

window.addEventListener("load", boxesClassSetup);

function boxesClassSetup() {
    loginBox.classList.remove("hidden");

    characterBox.classList.add("hidden");
    chooseRaceBox.classList.add("hidden");
    chooseClassBox.classList.add("hidden");
    chooseEquipmentBox.classList.add("hidden");
}

function checkString(stringToCheck) {
    return stringToCheck === null || stringToCheck.trim() === "";
}

function checkTextContent(elemToCheck, errorElem) {
    if (checkString(elemToCheck.value)) {
        errorElem.innerText = "Insert a correct Username";
        elemToCheck.value = "";
        return false;
    }
    return true;
}

function checkExistingUser() {
    if (!checkTextContent(userNameText, loginErrorText)) return;

    loginErrorText.innerText = "";
}

function formatUserName(userNameRead) {
    let userName = "";

    for (let i = 0; i < userNameRead.length; i++) {
        if (i === 0) {
            userName += userNameRead[i].toUpperCase();
        } else {
            userName += userNameRead[i].toLowerCase();
        }
    }

    return userName;
}

function createNewUser() {
    if (!checkTextContent(userNameText, loginErrorText)) return;

    const userName = formatUserName(userNameText.value);

    console.log(userName);
    userNameText.value = userName;

    const userCreated = new User(userName);
    changeBox(loginBox, characterBox);
}

function changeBox(prevElement, nextElement) {
    prevElement.classList.add("hidden");
    nextElement.classList.remove("hidden");
}
