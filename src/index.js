window.addEventListener("load", characterExistingCheck);

const charCreationPage = "./character-creation.html";
const gamePage = "./game-page.html";

function characterExistingCheck() {
    const charRetrieve = localStorage.getItem("character");
    if (charRetrieve === null) {
        window.location.href = charCreationPage;
    } else {
        window.location.href = gamePage;
    }
}
