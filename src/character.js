export class Character {
    constructor() {}

    setName(name) {
        this._name = name;
    }

    getName() {
        return this._name;
    }

    setRace(race) {
        this._race = race;
    }

    getRace() {
        return this._race;
    }

    setClass(charClass) {
        this._charClass = charClass;
    }

    getClass() {
        return this._charClass;
    }

    setEquipment(equipment) {
        this._equipment = equipment;
    }

    getEquipment() {
        return this._equipment;
    }

    static createFromJson(charObj) {
        const instance = new Character();

        instance.setName(charObj._name);
        instance.setClass(charObj._charClass);
        instance.setRace(charObj._race);
        instance.setEquipment(charObj._equipment);

        return instance;
    }
}
