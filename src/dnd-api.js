import { map_range } from "./custom-math.js";

const DND_API_BASE_URL = "https://www.dnd5eapi.co/";

async function getDataFromDndApi(endpoint, onCompleteFetch, onFailedFetch) {
    const url = new URL(endpoint, DND_API_BASE_URL);
    try {
        const result = await fetch(url);
        const data = await result.json();
        if (onCompleteFetch) {
            onCompleteFetch(data);
        }
    } catch (error) {
        if (onFailedFetch) {
            onFailedFetch(error);
        }
    }
}
export function getDndRaces(onCompled, onFailed) {
    getDataFromDndApi("api/races", onCompled, onFailed);
}

export function getDndClasses(onCompled, onFailed) {
    getDataFromDndApi("api/classes", onCompled, onFailed);
}
//////////////

/**
 * Call Dnd api and return the data in json format
 * @param {string} endpoint
 */
async function returnDataFromDndApi(endpoint) {
    const url = new URL(endpoint, DND_API_BASE_URL);

    try {
        const result = await fetch(url);
        let data;
        data = await result.json();
        return {
            ok: true,
            result: data,
        };
    } catch (error) {
        return {
            ok: false,
            result: error,
        };
    }
}

/**
 * @param {{id: string, weapons: string[], proficencies: string[]}} weaponByClass
 * @param {(error: Error) => void} onFailed
 * @returns {Promise<string[]>}
 */
export async function getDndWeapons(weaponByClass, onFailed) {
    const weaponList = [];
    for (const proficency of weaponByClass.proficencies) {
        let data = await returnDataFromDndApi(`api/equipment-categories/${proficency}`);

        if (!data.ok) {
            onFailed(data.result);
            break;
        }

        data.result.equipment.forEach((element) => {
            weaponList.push(element.index);
        });
    }
    return weaponList;
}

export async function getDndEnemy(challengeRatio, onFailed) {
    let cr = [];
    let minCr = 1;

    if (challengeRatio < 15) {
        cr = [0, 0.125, 0.25, 0.5];
    } else {
        minCr = 10;
    }

    for (let i = minCr; i <= challengeRatio; i++) {
        cr.push(i);
    }

    let data = await returnDataFromDndApi(`api/monsters?challenge_rating=${cr}`);

    if (!data.ok) {
        onFailed(data.result);
        return;
    } else {
        const rand = Math.random();
        const monsterIndex = Math.floor(map_range(rand, 0, 1, 0, data.result.results.length));
        const monster = await returnDataFromDndApi(`api/monsters/${data.result.results[monsterIndex].index}`);
        return monster.result;
    }
}

export function getDnDEnemyImage(imgUrl) {
    return new URL(imgUrl, DND_API_BASE_URL);
}
