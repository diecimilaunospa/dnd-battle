import { Character } from "./character.js";
import { getDndEnemy, getDnDEnemyImage } from "./dnd-api.js";
import { map_range as remapFloat } from "./custom-math.js";

const charInfo = document.getElementById("char-info");
const monsterInfo = document.getElementById("monster-info");

const playerImg = document.getElementById("player-img");
const monsterImg = document.getElementById("enemy-img");
const newEncounterBtn = document.getElementById("new-ecounter-btn");
const nextEncounterBtn = document.getElementById("next-ecounter-btn");
const rollAttackBtn = document.getElementById("roll-attack-btn");
const backToCharCreation = document.getElementById("back-to-char-creation");

const playerDice = document.getElementById("player-dice");
const monsterDice = document.getElementById("monster-dice");
const historyList = document.getElementById("history-list");
const roundResultText = document.getElementById("round-results-text");

const lastBestRoundField = document.getElementById("last-best-round-count");
const currentBestRoundField = document.getElementById("current-round-count");

newEncounterBtn.addEventListener("click", loadNewMonster);
nextEncounterBtn.addEventListener("click", loadNextMonster);

rollAttackBtn.addEventListener("click", rollAttack);
window.addEventListener("load", updateInfoOnScreen);

let currentRound = 0;
let bestRound = 0;
let enemyChallengeRating = 1;

function updateInfoOnScreen() {
    const characterJson = localStorage.getItem("character");
    const characterReader = JSON.parse(characterJson);

    const character = Character.createFromJson(characterReader);

    charInfosRetrieved.setPlayerInfos(
        character.getName(),
        character.getRace(),
        character.getClass(),
        character.getEquipment()
    );

    playerImg.src = `./assets/class_sprites/${character.getClass()}_class.png`;

    charInfo.innerHTML = charInfosRetrieved.getPlayerHTML();
    monsterInfo.innerHTML = monsterInfosRetrieved.getMonsterHTML();

    updateDisplayBestRound();
    updateDisplayCurrentRound();

    setDiceValue(playerDice, 20);
    setDiceValue(monsterDice, 20);

    monsterImg.src = "";
    clearHistory();

    roundResultText.innerText = "";
}

function updateDisplayBestRound() {
    if (currentRound > bestRound) {
        bestRound = currentRound;
    }

    lastBestRoundField.innerHTML = `Highest round reached: <span class"text-right">${bestRound}</span>`;
    updateDisplayCurrentRound();
}

function updateDisplayCurrentRound() {
    currentBestRoundField.innerHTML = `Current round: <span class"text-right">${currentRound}</span>`;
}

function resetMonsterDisplay() {
    monsterImg.src = "./assets/Loading_2.gif";
    monsterInfosRetrieved.setMonsterInfos("", "", "", "");
    monsterInfo.innerHTML = monsterInfosRetrieved.getMonsterHTML();
}

function loadNextMonster() {
    nextEncounterBtn.classList.add("hidden");
    loadMonster();
}

function loadNewMonster() {
    newEncounterBtn.classList.add("hidden");
    currentRound = 0;
    clearHistory();
    loadMonster();
}

function clearHistory() {
    historyList.innerHTML = "";
    historyList.innerText = "FIGHT HISTORY";
}

async function loadMonster() {
    roundResultText.innerText = "";

    setDiceValue(playerDice, 20);
    setDiceValue(monsterDice, 20);

    currentRound++;
    updateDisplayCurrentRound();

    resetMonsterDisplay();

    if (currentRound <= 4) {
        enemyChallengeRating = 1;
    } else if (currentRound > 30) {
        enemyChallengeRating = 30;
    } else {
        enemyChallengeRating = currentRound - 3;
    }

    const monster = await getDndEnemy(enemyChallengeRating, (error) => {
        handleFetchError(error, "monster");
    });

    if (monster.image != null) {
        monsterImg.src = getDnDEnemyImage(monster.image);
    } else {
        if (monster.type.includes("swarm", "beasts")) {
            monsterImg.src = `./assets/monsters_icons/beast.jpg`;
        } else {
            monsterImg.src = `./assets/monsters_icons/${monster.type}.jpg`;
        }
    }

    monsterInfosRetrieved.setMonsterInfos(monster.name, monster.type, monster.challenge_rating, monster.alignment);
    monsterInfo.innerHTML = monsterInfosRetrieved.getMonsterHTML();

    rollAttackBtn.classList.remove("hidden");
}

function rollAttack() {
    rollAttackBtn.classList.add("hidden");
    const playerRandom = Math.random();
    const enemyRandom = Math.random();

    const playerRoll = Math.floor(remapFloat(playerRandom, 0, 1, 1, 20));
    const enemyRoll = Math.floor(remapFloat(enemyRandom, 0, 1, 1, 20));

    // Set both Dices to 20
    setDiceValue(playerDice, 0);
    setDiceValue(monsterDice, 0);

    const diceRollingTimeout = setTimeout(() => {
        setDiceValue(playerDice, playerRoll);
        setDiceValue(monsterDice, enemyRoll);

        checkDiceRoll(playerRoll, enemyRoll);

        clearTimeout(diceRollingTimeout);
    }, 4);
}

function checkDiceRoll(playerRoll, monsterRoll) {
    if (playerRoll > monsterRoll) {
        nextEncounterBtn.classList.remove("hidden");

        roundResultText.innerText = "round Won!";
        roundResultText.style.color = "green";

        const historyElement = new HistoryElement(
            charInfosRetrieved.name,
            monsterInfosRetrieved.name,
            playerRoll,
            monsterRoll
        );

        historyList.appendChild(historyElement.getVictoryElement());
    }

    if (playerRoll < monsterRoll) {
        roundResultText.innerText = "round Lost!";
        roundResultText.style.color = "red";

        newEncounterBtn.classList.remove("hidden");

        const historyElement = new HistoryElement(
            charInfosRetrieved.name,
            monsterInfosRetrieved.name,
            playerRoll,
            monsterRoll
        );

        historyList.appendChild(historyElement.getLostElement());
        updateDisplayBestRound();
    }

    if (playerRoll === monsterRoll) {
        roundResultText.style.color = "black";
        roundResultText.innerText = "Draw!";

        rollAttackBtn.classList.remove("hidden");
        const historyElement = new HistoryElement(
            charInfosRetrieved.name,
            monsterInfosRetrieved.name,
            playerRoll,
            monsterRoll
        );

        historyList.appendChild(historyElement.getDrawElement());
        //DRAW!!
        //update history
    }
}

function setDiceValue(imgElement, diceValue) {
    let diceImgUrl = "";

    if (diceValue === 0) {
        diceImgUrl = `./assets/dice/DiceRolling.gif`;
    } else {
        if (diceValue < 10) {
            diceImgUrl = `./assets/dice/Dice_0${diceValue}.png`;
        } else {
            diceImgUrl = `./assets/dice/Dice_${diceValue}.png`;
        }
    }

    imgElement.src = diceImgUrl;
}

function handleFetchError(error, errorType) {
    console.log(`error fetching ${errorType}! error: `, error);

    if (errorType === "monster") {
        startEncounterBtn.classList.remove("hidden");
    }
}

class HistoryElement {
    constructor(playerName, monsterName, playerRoll, monsterRoll) {
        this.playerName = playerName;
        this.monsterName = monsterName;
        this.playerRoll = playerRoll;
        this.monsterRoll = monsterRoll;
    }

    getVictoryElement() {
        const newElement = document.createElement("li");
        newElement.innerHTML = `<span class="text-yellow-400">${this.playerName} killed ${this.monsterName} <span class="text-green-600">${this.playerRoll}</span> vs <span class="text-red-600">${this.monsterRoll}</span></span>`;
        return newElement;
    }

    getDrawElement() {
        const newElement = document.createElement("li");
        newElement.innerHTML = `<span class="text-white">${this.playerName} draw with ${this.monsterName} <span class="text-green-600">${this.playerRoll}</span> vs <span class="text-red-600">${this.monsterRoll}</span></span>`;
        return newElement;
    }

    getLostElement() {
        const newElement = document.createElement("li");
        newElement.innerHTML = `<span class="text-orange-600">${this.monsterName} killed ${this.playerName} <span class="text-green-600">${this.playerRoll}</span> vs <span class="text-red-600">${this.monsterRoll}</span></span>`;
        return newElement;
    }
}

class InfoBox {
    constructor() {}

    getPlayerHTML() {
        const innerHTML = `<strong><pre>Player info</strong>
        <strong>Name</strong>:      ${this.name}
        <strong>Race</strong>:      ${this.race}
        <strong>Class</strong>:     ${this.charClass}
        <strong>Weapon</strong>:    ${this.weapon}`;
        return innerHTML;
    }

    getMonsterHTML() {
        const innerHTML = `<strong><pre>Monster info</strong>
        <strong>Name</strong>:              ${this.name}
        <strong>Race</strong>:              ${this.race}
        <strong>Alignment</strong>:         ${this.alignment}
        <strong>Challenge Rating</strong>:  ${this.challengeRating}`;

        return innerHTML;
    }
    /**
     *
     * @param {string} name
     * @param {string} type
     * @param {string} challengeRating
     * @param {string} alignment
     */
    setMonsterInfos(name, type, challengeRating, alignment) {
        this.name = name;
        this.race = type;
        this.challengeRating = challengeRating;
        this.alignment = alignment;
    }

    /**
     *
     * @param {string} name
     * @param {string} race
     * @param {string} charClass
     * @param {string} weapon
     */
    setPlayerInfos(name, race, charClass, weapon) {
        this.name = name;
        this.race = race;
        this.charClass = charClass;
        this.weapon = weapon;
    }
}

const monsterInfosRetrieved = new InfoBox();
monsterInfosRetrieved.setMonsterInfos("", "", "", "");
const charInfosRetrieved = new InfoBox();
charInfosRetrieved.setPlayerInfos("", "", "", "");

backToCharCreation.addEventListener("click", backToCharacterCreation);

function backToCharacterCreation() {
    const charCreationPage = "./character-creation.html";

    window.location.href = charCreationPage;
}
