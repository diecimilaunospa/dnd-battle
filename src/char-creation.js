import { Character } from "./character.js";
import { getDndWeapons, getDndClasses, getDndRaces } from "./dnd-api.js";
import { getWeaponByClass } from "./weapon-container.js";

const gamePage = "./game-page.html";

const confirmCreationBtn = document.getElementById("confirm-creation-btn");

const charNameInput = document.getElementById("char-name-input");
const raceList = document.getElementById("race-list");
const classList = document.getElementById("class-list");
const equipmentList = document.getElementById("equipment-list");

const chooseRaceText = document.getElementById("choose-race-text");
const chooseClassText = document.getElementById("choose-class-text");
const chooseEquipmentText = document.getElementById("choose-equipment-text");

const charChoosenName = document.getElementById("character-name");
const raceChoosenName = document.getElementById("race-choosen-name");
const raceChoosenSprite = document.getElementById("race-choosen-sprite");
const classChoosenName = document.getElementById("class-choosen-name");
const classChoosenSprite = document.getElementById("class-choosen-sprite");
const equipmentChoosenText = document.getElementById("equipment-choosen-text");
const confirmError = document.getElementById("confirm-error");

confirmCreationBtn.addEventListener("click", confirmCreation);

raceList.addEventListener("click", selectRace);
classList.addEventListener("click", selectClass);
equipmentList.addEventListener("click", selectEquipment);

chooseRaceText.addEventListener("click", showRaceList);
chooseClassText.addEventListener("click", showClassList);
chooseEquipmentText.addEventListener("click", showEquipmentList);

charNameInput.addEventListener("change", updateChoosenName);
window.addEventListener("load", populateList);

let myChar;

function updateChoosenName(e) {
    if (e.target.value === "") {
        charChoosenName.innerText = "Insert a correct name";
        return;
    }

    console.log("updateChoosenName: ", e.target.value);
    charChoosenName.innerText = e.target.value;
}

function showClassList(e) {
    showElement(classList, [raceList, equipmentList]);
}

function showRaceList(e) {
    showElement(raceList, [classList, equipmentList]);
}

let weaponList = [];

async function showEquipmentList(e) {
    if (showElement(equipmentList, [classList, raceList])) {
        clearChilds(equipmentList);
        weaponList = [];
        const weapons = getWeaponByClass(myChar.getClass());

        weapons.weapons.forEach((element) => {
            weaponList.push(element);
        });

        if (weapons.proficencies.length > 0) {
            const weaponListGetted = await getDndWeapons(weapons, (error) => handleFetchError(error, "equipment"));
            weaponListGetted.forEach((element) => {
                if (!weaponList.includes(element)) weaponList.push(element);
            });
        }

        updateEquipmentList(weaponList);
    }
}

function updateEquipmentList(weapons) {
    for (const weapon of weapons) {
        let name = "";
        for (let i = 0; i < weapon.length; i++) {
            if (i === 0) {
                name += weapon[i].toUpperCase();
            } else {
                name += weapon[i].toLowerCase();
            }
        }

        const endpoint = `/api/equipment/${weapon}`;

        let classObj = new ListObject(weapon, name, endpoint, "equipment");
        equipmentList.appendChild(classObj.getRelatedElementAsSimpleButton());
    }
}

function setPlusIcon(elem) {
    elem.classList.remove("fa-minus");
    elem.classList.add("fa-plus");
}

function setMinusIcon(elem) {
    elem.classList.remove("fa-plus");
    elem.classList.add("fa-minus");
}

function showElement(elemToShow, elemToHide) {
    for (const elem of elemToHide) {
        elem.classList.add("hidden");
        setPlusIcon(elem.previousElementSibling.childNodes[1]);
    }

    if (elemToShow.classList.contains("hidden")) {
        elemToShow.classList.remove("hidden");
        setMinusIcon(elemToShow.previousElementSibling.childNodes[1]);
        return true;
    } else {
        elemToShow.classList.add("hidden");
        setPlusIcon(elemToShow.previousElementSibling.childNodes[1]);
        return false;
    }
}

function selectClass(e) {
    if (e.target.nodeName != "LI") return;
    selectCharInfo(e.target.getAttribute("id"), "class");
    selectCharInfo("", "equipment");
}

function selectRace(e) {
    if (e.target.nodeName != "LI") return;
    selectCharInfo(e.target.getAttribute("id"), "race");
    selectCharInfo("", "equipment");
}
function selectEquipment(e) {
    if (e.target.nodeName != "LI") return;

    selectCharInfo(e.target.getAttribute("id"), "equipment");
}
function selectCharInfo(index, type) {
    switch (type) {
        case "class":
            myChar.setClass(index);
            break;
        case "race":
            myChar.setRace(index);
            break;
        case "equipment":
            myChar.setEquipment(index);
            break;

        default:
            break;
    }

    setObjectChoosen(index, type);
}

function setObjectChoosen(index, objectType) {
    switch (objectType) {
        case "class":
            classChoosenName.innerText = index.toUpperCase();
            classChoosenSprite.src = getSpriteDir(objectType, index);
            break;
        case "race":
            raceChoosenName.innerText = index.toUpperCase();
            raceChoosenSprite.src = getSpriteDir(objectType, index);
            break;
        case "equipment":
            equipmentChoosenText.innerText = index.toUpperCase();
            break;

        default:
            break;
    }
}

function getSpriteDir(objType, objIndex) {
    return `./assets/${objType}_sprites/${objIndex}_${objType}.png`;
}

class ListObject {
    constructor(index, name, endpoint, objType) {
        this.index = index;
        this.name = name;
        this.endpoint = endpoint;
        this.objType = objType;
    }

    getRelatedElementWithImageChild() {
        const htmlClassList = [
            "list-element",
            `${this.objType}-element`,
            `bg-[url('${getSpriteDir(this.objType, this.index)}')]`,
        ];
        let newListObj = document.createElement("li");
        for (const htmlClass of htmlClassList) {
            newListObj.classList.add(htmlClass);
        }
        newListObj.setAttribute("id", this.index);

        newListObj.innerHTML += `
            <div class="hovered-image bg-[url('${getSpriteDir(this.objType, this.index)}')]
                    opacity-0">
            <p class="element-name">${this.index}</p></div>`;

        newListObj.addEventListener("mouseover", function (e) {
            let target;

            if (e.target.nodeName === "LI") {
                target = e.target.childNodes[1];
                target.classList.remove("opacity-0");
                target.classList.add("opacity-100");
            }
        });

        newListObj.addEventListener("mouseout", function (e) {
            let target;

            if (e.target.nodeName === "LI") {
                target = e.target.childNodes[1];
                target.classList.remove("opacity-100");
                target.classList.add("opacity-0");
            }
        });

        return newListObj;
    }

    static calcInnerText(text) {
        let result = "";
        for (let i = 0; i < text.length; i++) {
            if (i === 0 || (i - 1 > 0 && text[i - 1] === "-")) {
                result += text[i].toUpperCase();
            } else {
                result += text[i].toLowerCase();
            }
        }
        return result;
    }

    getRelatedElementAsSimpleButton() {
        let newListObj = document.createElement("li");
        newListObj.classList.add("list-element");
        newListObj.classList.add(`${this.objType}-element`);

        newListObj.setAttribute("id", this.index);
        newListObj.innerText = this.name;
        return newListObj;
    }
}

function handleFetchError(error, errorType) {
    console.log(`error fetching ${errorType}! error: `, error);
}

function populateList() {
    getDndRaces(
        function (elements) {
            updateListElemets(elements, "race");
        },
        (e) => handleFetchError(e, "races")
    );
    getDndClasses(
        function (elements) {
            updateListElemets(elements, "class");
        },
        (e) => handleFetchError(e, "classes")
    );

    myChar = new Character("");
}

function updateListElemets(listRetrieved, type) {
    switch (type) {
        case "race":
            clearChilds(raceList);
            break;
        case "class":
            clearChilds(classList);
            break;
        default:
            break;
    }

    for (const elem of listRetrieved.results) {
        const elemObj = new ListObject(elem.index, elem.name, elem.url, type);
        const elemToAppend = elemObj.getRelatedElementWithImageChild();

        switch (type) {
            case "race":
                raceList.appendChild(elemToAppend);
                break;
            case "class":
                classList.appendChild(elemToAppend);
                break;
            default:
                break;
        }
    }

    selectCharInfo(listRetrieved.results[0].index, type);
}

function clearChilds(element) {
    element.innerHTML = "";
}

function confirmCreation() {
    localStorage.removeItem("character");

    if (charNameInput.value === "") {
        handleConfirmationError("Insert a valid name");
        return;
    }
    if (myChar.getEquipment() === "" || myChar.getEquipment() === undefined) {
        handleConfirmationError("Equipment not selected");
        return;
    }
    handleConfirmationError("");

    myChar.setName(charNameInput.value);

    const charJson = JSON.stringify(myChar);

    localStorage.setItem("character", charJson);
    moveToGamePage();
}

function moveToGamePage() {
    const charRetrieve = localStorage.getItem("character");

    if (charRetrieve === null) {
        console.error("local storage not correctly retrieved, retry!");
        return;
    } else {
        window.location.href = gamePage;
    }
}

function handleConfirmationError(error) {
    confirmError.innerText = error;
}
