/**
 *
 * @param {number} value    // value to remap
 * @param {number} low1     // first value min
 * @param {number} high1    // first value max
 * @param {number} low2     // remapped value min
 * @param {number} high2    // remapped value max
 * @returns
 */

export function map_range(value, low1, high1, low2, high2) {
    return low2 + ((high2 - low2) * (value - low1)) / (high1 - low1);
}
