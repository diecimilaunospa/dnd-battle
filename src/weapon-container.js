const weaponsByClass = [
    {
        id: "barbarian",
        weapons: [],
        proficencies: ["simple-weapons", "martial-weapons"],
    },
    {
        id: "bard",
        weapons: ["longsword", "rapier", "shortsword", "hand-crossbow"],
        proficencies: ["simple-weapons"],
    },

    {
        id: "cleric",
        weapons: [],
        proficencies: ["simple-weapons"],
    },
    {
        id: "druid",
        weapons: ["club", "dagger", "javelin", "mace", "quarterstaff", "sickle", "spear", "sling", "scimitar"],
        proficencies: [],
    },
    {
        id: "fighter",
        weapons: [],
        proficencies: ["simple-weapons", "martial-weapons"],
    },
    {
        id: "monk",
        weapons: ["shortsword"],
        proficencies: ["simple-weapons"],
    },
    {
        id: "paladin",
        weapons: [],
        proficencies: ["simple-weapons", "martial-weapons"],
    },
    {
        id: "ranger",
        weapons: [],
        proficencies: ["simple-weapons", "martial-weapons"],
    },
    {
        id: "rogue",
        weapons: ["longsword", "rapier", "shortsword", "hand-crossbow"],
        proficencies: ["simple-weapons"],
    },
    {
        id: "sorcerer",
        weapons: ["dagger", "sling", "quarterstaff", "crossbows-light"],
        proficencies: [],
    },
    {
        id: "warlock",
        weapons: [],
        proficencies: ["simple-weapons"],
    },
    {
        id: "wizard",
        weapons: ["dagger", "sling", "quarterstaff", "crossbows-light"],
        proficencies: [],
    },
];

export function getWeaponByClass(classSelected) {
    for (let i = 0; i < weaponsByClass.length; i++) {
        if (weaponsByClass[i].id === classSelected) return weaponsByClass[i];
    }
    return null;
}
